package net.flyingfishflash.ledger.books.data.dto;

/** DTO representing a simple API Response */
public record ApiMessage(String message) {}
