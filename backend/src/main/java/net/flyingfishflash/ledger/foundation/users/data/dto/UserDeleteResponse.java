package net.flyingfishflash.ledger.foundation.users.data.dto;

public class UserDeleteResponse extends ApiMessage {

  public UserDeleteResponse(String message) {
    super(message);
  }
}
