package net.flyingfishflash.ledger.foundation.users.data.dto;

public class UserCreateResponse extends ApiMessage {

  public UserCreateResponse(String message) {
    super(message);
  }
}
