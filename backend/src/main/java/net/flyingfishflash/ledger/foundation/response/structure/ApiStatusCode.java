package net.flyingfishflash.ledger.foundation.response.structure;

public enum ApiStatusCode {
  SUCCESS,
  FAIL,
  ERROR
}
