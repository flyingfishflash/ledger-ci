package net.flyingfishflash.ledger.accounts.data.dto;

/** Record (DTO) class representing a simple API Response */
public record ApiMessage(String message) {}
