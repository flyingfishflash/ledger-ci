<h1><img src="meta/project-icon.png" alt="logo" width="45" height="45"> Ledger</h1>

[![pipeline status](https://gitlab.com/flyingfishflash/ledger/badges/main/pipeline.svg)](https://gitlab.com/flyingfishflash/ledger/-/pipelines/)
[![codecov](https://codecov.io/gl/flyingfishflash/ledger-ci/branch/main/graph/badge.svg?token=HOCJNHPN6Z)](https://codecov.io/gl/flyingfishflash/ledger-ci)
## Description

A Java based bookkeeping engine and API server, along with an Angular based web application for personal finance management.


## Run
A docker-compose file is supplied which can be used to run the application:
```shell
docker-compose -f docker-compose.yaml up -d
```
## API

* [http://localhost:52300/](http://localhost:52300/api/)
* Documentation: [http://localhost:52300/swagger-ui/](http://localhost:52300/swagger-ui/)

## Web Application

* [http://localhost:52301](https://localhost:52301)
